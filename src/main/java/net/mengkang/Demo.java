package net.mengkang;

import net.mengkang.rpc.RpcClient;
import net.mengkang.sdk.User;
import net.mengkang.sdk.UserService;

/**
 * Created by zhoumengkang on 2017/10/22.
 */
public class Demo {
    public static void demo(){
        RpcClient rpcClient = new RpcClient();
        UserService userService = (UserService) rpcClient.proxy(UserService.class);
        User user = userService.getUserInfo(10);
        System.out.println(user.getId());
        System.out.println(user.toString());
    }

    public static void main(String args[]){
        Demo.demo();
    }
}
